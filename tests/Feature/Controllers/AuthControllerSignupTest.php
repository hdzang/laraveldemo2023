<?php

namespace Controllers;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthControllerSignupTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * 基本註冊測試
     */
    public function test_signup(): void
    {
        // given 合法註冊資料
        $data = $this->generateValidSignupData();

        // when 送出註冊
        $response = $this->post('signup', $data);

        // then 驗證成功建立使用者
        $response->assertCreated();
    }

    public function test_signup_passwordConfirmationNotMatch_returnsError(): void
    {
        // given 不一致的密碼
        $data = $this->generateValidSignupData();
        $data['password_confirmation'] = $data['password'] . '123';

        // when 送出註冊
        $request = $this->post('signup', $data);

        // then 預期返回錯誤
        $request->assertOk();
        $request->assertJsonValidationErrors(['password' => 'The password field confirmation does not match.']);
    }

    /**
     * @return string[] 合法的註冊資料
     */
    private function generateValidSignupData(): array
    {
        $password = '25890a90294c44f1a2fd9815aef180ec';
        return [
            'name' => 'Tony',
            'email' => 'tony@gmail.com',
            'password' => $password,
            'password_confirmation' => $password
        ];
    }

}
