<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignupUserRequest;
use App\Services\User\UserServiceImplement;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    private UserServiceImplement $userService;

    public function __construct(UserServiceImplement $userService)
    {
        $this->userService = $userService;
    }

    /**
     * 註冊
     * @param SignupUserRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function signup(SignupUserRequest $request)
    {
        $validatedData = $request->validated();

        $this->userService->signup($validatedData);

        return response('ok', 201);
    }
}
