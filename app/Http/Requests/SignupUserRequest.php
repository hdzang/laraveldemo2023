<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupUserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|unique:users|email',
            'password' => [
                'required',
                'string',
                'confirmed',
                'min:10',
                'regex:/[a-zA-Z]/',   // 至少含一碼英文字母
                'regex:/[0-9]/',      // 至少含一碼數字
            ],
        ];
    }
}
